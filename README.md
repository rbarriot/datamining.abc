# Fouille de données sur les systèmes ABC chez les prokaryotes

![Transporteur ABC](rapport/transporteurs.png)

# Objectifs

Analyse de classification ou de clustering à définir

# Résultats phares

Mise en avant des résultats obtenus lors de l'analyse.

# Liens

- Slides de la présentation des données : https://docs.google.com/presentation/d/158SeOGoGEgRE-wVr7RTnHPzhRFEGiQCOCw5FaZJhSQ0
- Description des données fournies : http://silico.biotoul.fr/enseignement/m1/datamining/projet/DataMining.ABC.Presentation.des.donnees.html
- Données: https://silico.biotoul.fr/enseignement/m1/datamining/projet/data/

